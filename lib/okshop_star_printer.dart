
import 'dart:async';

import 'package:flutter/services.dart';

class OkshopStarPrinter {
  static const MethodChannel _channel =
      const MethodChannel('okshop_star_printer');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
