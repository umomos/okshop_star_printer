import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_star_printer/okshop_star_printer.dart';

void main() {
  const MethodChannel channel = MethodChannel('okshop_star_printer');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await OkshopStarPrinter.platformVersion, '42');
  });
}
