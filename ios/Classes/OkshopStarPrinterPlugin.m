#import "OkshopStarPrinterPlugin.h"
#if __has_include(<okshop_star_printer/okshop_star_printer-Swift.h>)
#import <okshop_star_printer/okshop_star_printer-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "okshop_star_printer-Swift.h"
#endif

@implementation OkshopStarPrinterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftOkshopStarPrinterPlugin registerWithRegistrar:registrar];
}
@end
